package org.rlam;

import javax.persistence.Entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
public class customers extends PanacheEntity {
    public String first_name;
    public String last_name;
    public String email;

    public customers() {

    }

    public customers(String first_name, String last_name, String email) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
    }
}