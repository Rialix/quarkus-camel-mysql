package main

import data.kubernetes

name = input.metadata.name

deny[msg] {
    input.kind = "Deployment"
    input.spec.template.spec.securityContext.runAsNonRoot
    msg = sprintf("Containers must run as root in Deployment %s", [name])
}

required_deployment_selectors {
    input.spec.selector.matchLabels.app
    input.spec.selector.matchLabels.release
}

deny [msg] {
    input.kind = "Deployment"
    required_deployment_selectors

    msg = sprintf("Deployment %s must provide app/release labels for pod selectors", [name])
}